<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Group::insert([
            ['name' => 'Clusters', 'key' => 'clusters', 'description' => 'Cluster description goes here'],
            ['name' => 'Hubs/Co-Working Spaces', 'key' => 'hubs', 'description' => 'Cluster description goes here'],
            ['name' => 'Startups', 'key' => 'startups', 'description' => 'Cluster description goes here'],
        ]);
    }
}
