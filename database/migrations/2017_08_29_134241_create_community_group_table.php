<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityGroupTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_group', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('community_id');
            $table->unsignedTinyInteger('group_id');
            $table->timestamps();

            $table->foreign('group_id', 'community_group_group_id')->references('id')->on('groups')
                    ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('community_id', 'community_group_community_id')->references('id')->on('communities')
                    ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_group');
    }

}
