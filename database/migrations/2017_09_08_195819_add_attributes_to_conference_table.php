<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToConferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conference', function (Blueprint $table) {
            $table->string('photo', 2000)->nullable();
            $table->text('description');
            $table->string('city');
            $table->unsignedInteger('state_id');
            $table->softDeletes();
            
            $table->foreign('state_id', 'conference_state_id')->references('id')->on('states')
                    ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conference', function (Blueprint $table) {
            //
        });
    }
}
