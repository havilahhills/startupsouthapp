<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('community_id');
            $table->boolean('primary')->default(false);
            $table->timestamps();
            
            $table->foreign('user_id', 'community_user_user_id')->references('id')->on('users')
                    ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('community_id', 'community_user_community_id')->references('id')->on('communities')
                    ->onDelete('cascade')->onUpdate('cascade');
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_user');
    }
}
