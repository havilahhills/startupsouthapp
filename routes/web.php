<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::get('/test', function() {
    $beautymail = app()->make(Snowfire\Beautymail\Beautymail::class);
    $beautymail->send('emails.welcome', [], function($message) {
        $message
                ->from('bar@example.com')
                ->to('foo@gmail.com', 'John Smith')
                ->subject('Welcome!');
    });
});




Route::group(['as' => 'home.'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);
    Route::get('/about', ['as' => 'about', 'uses' => 'HomeController@getAbout']);
    Route::get('/conference', ['as' => 'conference', 'uses' => 'HomeController@getConference']);
    Route::get('/angel-network', ['as' => 'angel-network', 'uses' => 'HomeController@getAngelNetwork']);
    Route::get('/vc-tours', ['as' => 'vc-tours', 'uses' => 'HomeController@getVcTours']);

    Route::group(['as' => 'community.', 'prefix' => 'community'], function() {
        Route::post('/create', ['as' => 'create', 'uses' => 'CommunityController@create']);
        Route::post('/photo', ['as' => 'photo', 'uses' => 'CommunityController@uploadPhoto']);
        Route::get('/{tab?}', ['as' => 'index', 'uses' => 'HomeController@getCommunity']);
    });

    Route::group(['as' => 'event.', 'prefix' => 'event'], function() {
        Route::post('create', ['as' => 'create', 'uses' => 'EventController@create']);
        Route::post('photo', ['as' => 'photo', 'uses' => 'EventController@uploadPhoto']);
        Route::post('respond', ['as' => 'respond', 'uses' => 'EventController@respond']);
        Route::get('{tab?}', ['as' => 'index', 'uses' => 'EventController@showEvents']);
    });
});

// Authorization
Route::get('/login', ['as' => 'auth.login.form', 'uses' => 'Auth\SessionController@getLogin']);
Route::post('/login', ['as' => 'auth.login.attempt', 'uses' => 'Auth\SessionController@postLogin']);
Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\SessionController@getLogout']);

// Registration
Route::get('register', ['as' => 'auth.register.form', 'uses' => 'Auth\RegistrationController@getRegister']);
Route::post('register', ['as' => 'auth.register.attempt', 'uses' => 'Auth\RegistrationController@postRegister']);

// Activation
Route::get('activate/{code}', ['as' => 'auth.activation.attempt', 'uses' => 'Auth\RegistrationController@getActivate']);
Route::get('resend', ['as' => 'auth.activation.request', 'uses' => 'Auth\RegistrationController@getResend']);
Route::post('resend', ['as' => 'auth.activation.resend', 'uses' => 'Auth\RegistrationController@postResend']);

// Password Reset
Route::get('password/reset/{code}', ['as' => 'auth.password.reset.form', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('password/reset/{code}', ['as' => 'auth.password.reset.attempt', 'uses' => 'Auth\PasswordController@postReset']);
Route::get('password/reset', ['as' => 'auth.password.request.form', 'uses' => 'Auth\PasswordController@getRequest']);
Route::post('password/reset', ['as' => 'auth.password.request.attempt', 'uses' => 'Auth\PasswordController@postRequest']);

// Users
Route::resource('users', 'UserController');

// Roles
Route::resource('roles', 'RoleController');

// Dashboard
Route::get('dashboard', ['as' => 'dashboard', 'uses' => function() {
        return view('centaur.dashboard');
    }]);

//Admin Routes
Route::get('add-event', ['as' => 'admin.add.event', 'uses' => 'EventController@addEvent']);



//View users, events and communities profiles
Route::get('{slug}', 'ProfileController@view')->name('profile');
