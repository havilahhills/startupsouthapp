<?php

define('DS', DIRECTORY_SEPARATOR);

function normalizeUrl($url)
{
    return str_replace('\\', '/', urldecode($url));
}

function normalizePath($path)
{
    return str_replace('/', DS, $path);
}

function inferCDN($asset, $prefix = 'public')
{
    $nasset = normalizeUrl($asset);
    if (env('USE_CDN') && preg_match("/^(.*)\.(.*)\/(.*)\/$prefix(.*)/", $nasset)) {
        $base = preg_replace("/^(.*)\.(.*)\/(.*)\/$prefix/", '', $nasset);
        $url = cdn($base, $prefix);
    } else {
        $url = asset(str_replace('storage/public', 'storage', $nasset));
    }
    return $url;
}

/**
 * global CDN link helper function
 * @param string $asset
 * @return string
 */
function cdn($asset, $prefix = '')
{
    // Get file name incl extension and CDN URLs
    $cdns = config('app.cdn');

    // Verify if CDN URLs are present in the config file
    if (empty($cdns)) {
        return;
    }

    // Select the CDN URL based on the prefix
    foreach ($cdns as $cdn => $path) {
        if (strcasecmp($prefix, $path) === 0) {
            return cdnPath($cdn, $asset);
        }
    }

    // In case of no match use the last in the array
    end($cdns);
    return cdnPath(key($cdns), $asset);
}

function cdnPath($cdn, $asset)
{
    return "//" . rtrim($cdn, "/") . "/" . ltrim($asset, "/");
}


function jsonOrRedirect($data = [], $cache = false)
{
    if (request()->wantsJson()) {
        $response = response()->json($data);
        if ($cache) {
            return $response->header('cache-control', 'public');
        } else {
            return $response;
        }
    } else {
        return redirect()->back()->withInput($data);
    }
}

function fixUrl($link){
    if(!empty($link)){
        $scheme = parse_url($link, PHP_URL_SCHEME);
        if (empty($scheme)) {
            $link = 'http://' . ltrim($link, '/');
        }
    }
    return $link;
}
