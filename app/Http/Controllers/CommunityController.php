<?php

namespace App\Http\Controllers;

use App\Models\Community;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommunityController extends Controller
{

    public function create(Request $request)
    {
        $this->validateData($request);
        return DB::transaction(function() use ($request) {
                    //Create or update user
                    $user = $this->createOrUpdateUser([
                        'email' => $request->email,
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'phone' => $request->phone
                    ], true);

                    //Create community
                    $community = $this->createCommunity($request, $user);

                    if (empty($request->admin)) {
                        $admin_user = $this->createOrUpdateUser([
                            'email' => $request->admin_email,
                            'first_name' => $request->admin_first_name,
                            'last_name' => $request->admin_last_name,
                            'phone' => $request->admin_phone
                        ]);
                        $community->users()->attach(
                                [
                                    ['user_id' => $user->id, 'primary' => false],
                                    ['user_id' => $admin_user->id, 'primary' => true]
                                ]
                        );
                    } else {
                        $community->users()->attach($user->id, ['primary' => true]);
                    }


                    return jsonOrRedirect($community->id);
                });
    }

    public function uploadPhoto(Request $request)
    {
        $community = Community::find($request->id);
        $community->handlePhotoUpload($request, 'community_photo');
        return jsonOrRedirect(['status' => true]);
    }

    private function validateData(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'community_name' => 'required',
            'community_city' => 'required',
            'community_state' => 'required|exists:states,id',
            'community_photo' => 'image',
            'community_groups' => 'required|array',
            'admin_first_name' => 'required_without:admin',
            'admin_last_name' => 'required_without:admin',
            'admin_phone' => 'required_without:admin',
            'admin_email' => 'required_without:admin',
        ]);
    }

    /**
     *
     * @param type $data
     * @return User
     */
    private function createOrUpdateUser($data, $login = false)
    {
        $user = User::findByEmail($data['email']);
        $new = false;
        if (!is_object($user)) {
            $user = new User;
            $user->email = $data['email'];
            $new = true;
        }
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->phone = $data['phone'];
        $user->save();

//        if ($login) {
//            auth()->login($user);
//        }

        if ($new) {
            //Send welcome email to user here
        }

        return $user;
    }

    private function createCommunity($request, $user)
    {
        $community = new Community;
        $community->name = $request->community_name;
        $community->state_id = $request->community_state;
        $community->city = $request->community_city;
        $community->description = $request->community_description;
        $community->address = $request->community_address;
        $community->website = $request->community_website;
        $community->created_by = $user->id;
        $community->save();
        $community->groups()->attach($request->community_groups);
        //Send email for community here
        return $community;
    }

}
