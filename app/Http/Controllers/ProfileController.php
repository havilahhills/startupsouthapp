<?php

namespace App\Http\Controllers;

use App\Models\Community;
use App\Models\Event;
use App\Models\Slug;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller {

    public function view(Request $request, $slug) {
        $slugModel = Slug::findBySlug($slug)->first();
        if (is_object($slugModel)) {
            $model = $slugModel->model;
            if ($model instanceof Community) {
                $data['community'] = $model;
                return view('profile.community', $data);
            } else if ($model instanceof User) {
                $data['user'] = $model;
                return view('profile.user', $data);
            } else if ($model instanceof Event) {
                $data['event'] = $model;
                return view('profile.event', $data);
            }
        }

        abort(404);
    }

}
