<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\State;

class EventController extends Controller
{
    
    
     public function addEvent()
    {
       $states = State::all();
        
        return view('admin.add-event', compact('states'));
    }
    
    
    
    public function add(Request $request) {
        
        $this->validate($request, [
            'name' => 'required',
            'venue' => 'required',
            'date' => 'required',
            'description' => 'required',
            'city' => 'required',

            ]);
            
            $conference = Event::create([
            'name' => 'required',
            'venue' => 'required',
            'date' => 'required',
            'website' => 'required',
            'photo' => 'required',
            'description' => 'required',
            'city' => 'required',
            ]);
            
            $conference->save();
            
            return redirect()->back()->with('status', 'New Event Created');
    }
    
    public function uploadPhoto(Request $request)
    {
        $community = Community::find($request->id);
        $community->handlePhotoUpload($request, 'community_photo');
        return jsonOrRedirect(['status' => true]);
    }
}
