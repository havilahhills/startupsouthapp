<?php

namespace App\Http\Controllers;

use App\Models\Community;
use App\Models\Group;
use App\Models\State;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }

    public function getConference() {
        
        return view('home.conference');
    }
    
    public function getAngelNetwork() {
        
        return view('home.angel');
    }
    
    public function getCommunity(Request $request, $tab = null) {
        $groups = Group::all();
        if(empty($tab) || $tab === 'all'){
            $tab = 'all';
            $group = null;
            $communities = Community::approved()->paginate();
        }else{
            $group = $groups->where('key', $tab)->first();
            if (is_object($group)) {
                $communities = $group->communities()->approved()->paginate();
            }else{
                $communities = collect();
            }
        }
        
        $data = [
            'tab'=>$tab,
            'communities' => $communities,
            'currentGroup' => $group,
            'groups' => $groups,
            'states' => State::all()
        ];
        return view('home.community', $data);
    }
    
    public function getVcTours() {
        
         return view('home.vc');
    }
    
    public function getAbout() {
        return view('about');
    }
}
