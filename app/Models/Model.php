<?php

namespace App\Models;

use App\Models\Traits\ModelHelpers;
use Exception;
use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * Class Model
 *
 * @package App\Models
 */
abstract class Model extends EloquentModel
{
    use ModelHelpers;

    public function cleanUp()
    {
        if (method_exists($this, 'deletePhoto')) {
            //Unlink photo
            $this->deletePhoto();

            //Unlink thumbnail
            $this->deleteThumbnail();
        }
    }

    public function scopeRejected($query)
    {
        return $query->where($this->getTable() . '.rejected_at', '<>', null);
    }

    public function scopeUnrejected($query)
    {
        return $query->where($this->getTable() . '.rejected_at', null);
    }

    public function scopeApproved($query)
    {
        return $query->where($this->getTable() . '.approved_at', '<>', null);
    }

    public function scopeUnapproved($query)
    {
        return $query->where($this->getTable() . '.approved_at', null);
    }

    public function scopeVerified($query)
    {
        return $query->where($this->getTable() . '.verified_at', '<>', null);
    }

    public function scopeUnverified($query)
    {
        return $query->where($this->getTable() . '.verified_at', null);
    }

}
