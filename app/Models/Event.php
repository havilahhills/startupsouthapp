<?php

namespace App\Models;

use App\Models\Traits\HasPhoto;
use App\Models\Traits\HasSlug;
use App\Models\Traits\SoftDeletes;
use App\Models\Traits\HasURL;

class Event extends Model
 {

    use HasURL;
    use HasSlug;
    use SoftDeletes;
    use HasPhoto;
    
    protected $table = "conference";

    protected $fillable = ['name', 'venue', 'date', 'website',];
    
    public function state()
    {
        return $this->belongsTo(State::class);
    }
    
    public function getCityAttribute($city)
    {
        return ucfirst(strtolower($city));
    }
}
