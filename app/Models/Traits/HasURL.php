<?php

namespace App\Models\Traits;

trait HasURL {

    protected function getUrlAttribute($url)
    {
        return route("profile", ['slug' => $this->slug]);
    }

}
