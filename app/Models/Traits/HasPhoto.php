<?php

namespace App\Models\Traits;

use App\Models\Community;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class GetPhotoUrl
 *
 * @package App\Models\Traits
 */
trait HasPhoto {

    private $imageDir;

    /**
     * Handle photo upload
     * @param Request $request
     * @param string $name name of form element
     */
    public function handlePhotoUpload(Request $request, $name = 'photo')
    {
        $this->deletePhoto();
        $this->deleteThumbnail();

        $uploadedFile = $request->file($name);
        $name = uniqid(str_replace(' ', '-', $this->name) . '-') . '.' . $uploadedFile->extension();
        $subdir = date('W-Y');
        $path = $subdir . DS . $name;
        $uploadedFile->storePubliclyAs($this->getImageDir(), $path);
        $this->photo = $path;
        $this->save();
    }

    private function getImageDir($path = '')
    {
        if (empty($this->imageDir)) {
            $class = get_class();
            $s = explode('\\', $class);
            $clasname = array_pop($s);
            $this->imageDir = 'public' . DS . strtolower($clasname);
        }

        if (!empty($path)) {
            return rtrim($this->imageDir, DS) . DS . trim($path, DS);
        } else {
            return $this->imageDir;
        }
    }

    private function getPhotoPath()
    {
        return $this->getImageDir($this->photo);
    }

    private function getThumbDir($path = '') {
        return $this->getImageDir('thumbs' . DS . $path);
    }

    private function getPhotoThumbPath() {
        return $this->getThumbDir($this->photo);
    }

    /**
     * @param $a
     *
     * @return string
     */
    protected function getPhotoUrlAttribute($a)
    {
        return $this->getPhotoUrl();
    }

    /**
     * @return string
     */
    public function getPhotoUrl()
    {
        if (empty($this->photo) || !Storage::exists($this->getPhotoPath())) {
            return $this->defaultPhotoUrl();
        }

        return inferCDN(Storage::url($this->getPhotoPath()));
    }

    /**
     * @return string
     */
    public static function defaultPhotoUrl()
    {
        $img = 'community.jpg';
        switch (self::class) {
            case User::class :
                $img = 'user.jpg';
                break;
            case Group::class :
                $img = 'group.jpg';
                break;
        }

        return asset('img/defaults/' . $img);
    }

    /**
     *
     */
    public function deletePhoto()
    {
        if (!empty($this->photo) && Storage::exists($this->getPhotoPath())) {
            Storage::delete($this->getPhotoPath());
        }
    }

    /**
     * @param $a
     *
     * @return string
     */
    protected function getThumbAttribute($a)
    {
        return $this->getThumbnail();
    }

    /**
     *
     */
    public function deleteThumbnail()
    {
        if (!empty($this->photo) && Storage::exists($this->getPhotoThumbPath())) {
            Storage::delete($this->getPhotoThumbPath());
        }
    }

    /**
     * Get or Create thumbnail
     *
     * @param boolean $createNew Force to create new thumbnail
     * @param string $old        Old URL
     *
     * @return string URL of thumbnail. Thumbnail is created if it does not exist
     * else old image URL is returned if conversion failed
     */
    public function getThumbnail($createNew = false, $old = '')
    {
        $destination = $this->getPhotoThumbPath();
        //Return thumbnail if it exists
        if (!empty($this->photo) && Storage::exists($destination) && !$createNew) {
            return inferCDN(Storage::url($destination));
        }

        //Create new
        $file = $this->getPhotoPath();
        if (!empty($this->photo) && Storage::exists($file)) {
            $max_size = $this instanceof Community ? 100 : 50;
            $jpeg_quality = 90;
            $old = empty($old) ? $old : $this->getThumbDir($old);
            if (Storage::exists($old)) {
                Storage::delete($old);
            }

            $local_file = storage_path('app' . DS . $file);
            $local_thumb = storage_path('app' . DS . $destination);
            Storage::disk('local')->put($file, Storage::get($file));
            $url = $this->resize_image($local_file, $local_thumb, $max_size, $jpeg_quality);
            Storage::put($destination, Storage::disk('local')->get($destination), 'public');
            Storage::disk('local')->delete([$file, $destination]);

            return $url ? inferCDN(Storage::url($destination)) : $this->getPhotoUrl();
        }

        return self::defaultPhotoUrl();
    }

    /**
     * Proportionally resize image
     *
     * @param string $source      Source image URL
     * @param string $destination Destination image URL
     * @param float $max_size     Maximum size
     * @param int $quality        Jpeg image quality
     *
     * @return boolean
     */
    private function resize_image($source, $destination, $max_size, $quality)
    {
        $image_size_info = getimagesize($source); //get image size
        if ($image_size_info) {
            $image_width = $image_size_info[0]; //image width
            $image_height = $image_size_info[1]; //image height
        } else {
            return false;
        }

        //return false if nothing to resize
        if ($image_width <= 0 || $image_height <= 0) {
            return false;
        }
        //do not resize if image is smaller than max size
        if ($image_width <= $max_size && $image_height <= $max_size) {
            return false;
        }

        //Construct a proportional size of new image
        $image_scale = min($max_size / $image_width, $max_size / $image_height);
        $new_width = ceil($image_scale * $image_width);
        $new_height = ceil($image_scale * $image_height);

        //Create a new true color image
        $new_image = imagecreatetruecolor($new_width, $new_height);
        //White background, no transparency
        $trans_colour = imagecolorallocate($new_image, 255, 255, 255);
        imagefill($new_image, 0, 0, $trans_colour);

        //Copy and resize part of an image with resampling
        $sourceResource = $this->getImageResource($source);
        if (imagecopyresampled($new_image, $sourceResource, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height)) {
            return $this->saveImage($new_image, $destination, $image_size_info['mime'], $quality);
        } else {
            return false;
        }
    }

    /**
     * @param $x
     * @param $y
     * @param $width
     * @param $height
     *
     * @return bool
     */
    public function cropImage($x, $y, $width, $height)
    {
        //return false if nothing to resize
        if ($width <= 0 || $height <= 0 || is_nan($x) || is_nan($y)) {
            return false;
        }
        $file = storage_path('app' . DS . $this->getPhotoPath());
        //Create a new true color image
        $new_image = imagecreatetruecolor($width, $height);
        //Retain transparency
        imagesavealpha($new_image, true);
        $trans_colour = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
        imagefill($new_image, 0, 0, $trans_colour);
        //Copy and resize part of an image with resampling
        $source = $this->getImageResource($file);
        if ($source) {
            if (imagecopyresampled($new_image, $source, 0, 0, $x, $y, $width, $height, $width, $height)) {
                $dest = storage_path('app' . DS . 'RAM' . DS . $this->getPhotoPath());
                if ($this->saveImage($new_image, $dest, getimagesize($file)['mime'], 50)) {
                    Storage::delete($this->getPhotoPath());

                    return Storage::move('RAM/' . $this->getPhotoPath(), $this->getPhotoPath());
                } else {
                    return false;
                }
            }
        }

        return false;
    }

    /**
     * @param resource $source    Image resource
     * @param string $destination Destination file
     * @param string $mime        Source mime type
     * @param int $quality        Jpeg quality (for jpeg images)
     *
     * @return boolean
     */
    private function saveImage($source, $destination, $mime, $quality)
    {
        //Create directory
        $split = explode(DS, str_replace('/', DS, $destination));
        $filename = array_pop($split); //Remove filename
        $dir = implode(DS, $split);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        //save resized image
        switch (strtolower($mime)) {
            case 'image/png':
                $ok = imagepng($source, $destination);
                break; //save png file
            case 'image/gif':
                $ok = imagegif($source, $destination);
                break; //save gif file
            case 'image/jpeg':
            case 'image/pjpeg': //Save jpg/jpeg file
                $ok = imagejpeg($source, $destination, $quality);
                break; //save jpeg file
            default:
                return false;
        }
        if ($ok) {
            return $destination;
        }
        unlink($destination);

        return false;
    }

    /**
     * @param $source
     *
     * @return bool|resource
     */
    private function getImageResource($source)
    {
        $sourceResource = false;
        $imageInfo = is_file($source) ? getimagesize($source) : false;
        if ($imageInfo) {
            switch (strtolower($imageInfo['mime'])) {
                case 'image/png':
                    $sourceResource = imagecreatefrompng($source);
                    break;
                case 'image/gif':
                    $sourceResource = imagecreatefromgif($source);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    $sourceResource = imagecreatefromjpeg($source);
                    break;
            }
        }

        return $sourceResource;
    }

}
