<?php

namespace App\Models\Traits;

use App\Models\Slug;

trait HasSlug {

    public function slugMorph()
    {
        return $this->morphMany(Slug::class, 'slugable');
    }

    protected function getSlugAttribute()
    {
        return $this->slugMorph()->first()->slug;
    }

    protected static function bootHasSlug()
    {
        //Create slug for this model
        static::created(function($model) {
            Slug::generateFor($model);
        });
    }

}
