<?php

namespace App\Models;

use App\Models\Traits\HasPhoto;
use App\Models\Traits\HasSlug;
use App\Models\Traits\SoftDeletes;
use App\Models\Traits\HasURL;

class Community extends Model
 {

    use HasURL;
    use HasSlug;
    use SoftDeletes;
    use HasPhoto;

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }


    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function getCityAttribute($city)
    {
        return ucfirst(strtolower($city));
    }

}
