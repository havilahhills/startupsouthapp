<?php

namespace App\Models;

class Group extends Model
{
    public function communities()
    {
        return $this->belongsToMany(Community::class);
    }

}
