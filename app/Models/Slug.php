<?php

namespace App\Models;

use App\Models\Traits\SoftDeletes;

class Slug extends Model
 {

    use SoftDeletes;

    protected $fillable = ['slug'];

    public function model()
    {
        return $this->morphTo('slugable');
    }

    public static function generateFor($model)
    {
        $exist = self::where([
                    'slugable_id' => $model->id,
                    'slugable_type' => $model::morphKey()
                ])->first();

        if (!is_object($exist)) {
            $slug = self::makeUniqueSlug($model->name);
            $model->slugMorph()->save(new static(['slug' => $slug]));
        }
    }

    /**
     * @param $string
     * @param $pattern
     *
     * @return mixed
     */
    public static function findBySlug($string, $pattern = false)
    {
        if ($pattern) {
            return self::where('slug', 'LIKE', $string);
        }

        return self::where('slug', $string);
    }

    /**
     * @param $title
     *
     * @return string
     */
    public static function makeUniqueSlug($title, $old = null)
    {
        $slug = self::makeSlug($title);
        if ($slug !== $old) {
            $matches = self::findBySlug($slug . '%', true)->withTrashed()->get()->count();
            if ($matches) {
                $temp = $slug . '-' . $matches;
                if (is_object(self::findBySlug($temp)->first())) {
                    $slug .= '-' . uniqid();
                } else {
                    $slug = $temp;
                }
            }
        }
        return $slug;
    }

    /**
     * @param $string
     *
     * @return string
     */
    public static function makeSlug($string)
    {
        $slug = str_slug($string);

        /* UTF8 requires 3 bytes per character to store the string,
         * so in your case 20 + 500 characters = 20*3+500*3 = 1560 bytes which is
         * more than allowed 767 bytes. The limit for UTF8 is 767/3 = 255 characters,
         * for UTF8mb4 which uses 4 bytes per character it is 767/4 = 191 characters
         *
         * We offset 1 character to use 190 characters, just because we can
         *
         * Since we're hoping to carry 30 million users (and more), we can imagine everyone
         * chose the same slug
         * extimated_max_slug_occurrence = 30,000,000
         *
         * For suffix, it come with an hyphen and either a uniqid() or number of occurence
         * len_of_uniqid = strlen(uniqid()) = 13
         * len_of_ext_max_slug_occurrence = strlen(extimated_max_slug_occurrence) = 8
         * len_of_hyphen = strlen('-') = 1
         *
         * suffix_length = max(len_of_uniqid,len_of_ext_max_slug_occurrence) + len_of_hyphen = 14
         *
         * Therefore, allowed slug length (without suffix) is 190 - 14 = 176
         */

        return substr($slug, 0, 176);
    }

}
