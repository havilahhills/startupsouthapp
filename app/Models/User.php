<?php

namespace App\Models;

use App\Models\Traits\HasName;
use App\Models\Traits\HasPhoto;
use App\Models\Traits\HasSlug;
use App\Models\Traits\ModelHelpers;
use App\Models\Traits\SoftDeletes;
use App\Models\Traits\HasURL;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

    use Notifiable;
    use HasName;
    use HasURL;
    use HasSlug;
    use SoftDeletes;
    use HasPhoto;
    use ModelHelpers;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function communities()
    {
        return $this->belongsToMany(Community::class);
    }

    /**
     * @return string
     */
    public function name($with_middle_name = true)
    {
        $name = $this->first_name;
        if (!empty($this->middle_name) and $with_middle_name) {
            $name .= " {$this->middle_name}";
        }
        $name .= " {$this->last_name}";

        return $name;
    }

    public static function findByEmail($email)
    {
        return self::where('email', $email)->first();
    }

    public function communitiesCreated()
    {
        return $this->hasMany(Community::class, 'created_by');
    }

}
