<?php ?>

@extends('layouts.app')

@section('title', 'Angel Network - #StartupSouth')

@section('content')
<h3 class='left-align'>Angel Network</h3>
<p class='left-align'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore
magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
quis nostrud exerci tation ullamcorper suscipit lobortis nisl
ut aliquip ex ea commodo consequat. Duis autem vel eum iriure
dolor in hendrerit in vulputate velit esse molestie consequat,
vel illum dolore eu feugiat nulla facilisis at vero eros et
accumsan et iusto odio dignissim qui blandit praesent luptatum
zzril delenit augue duis dolore te feugait nulla facilisi.
Nam liber tempor cum soluta nobis eleifend option congue
nihil imperdiet doming id quod mazim placerat facer possim
assum. Typi non habent claritatem insitam; est usus legentis
in iis qui facit eorum claritatem. Investigationes
demonstraverunt lectores legere me lius quod ii legunt saepius.
Claritas est etiam processus dynamicus, qui sequitur mutationem
consuetudium lectorum. Mirum est notare quam littera gothica,
quam nunc putamus parum claram, anteposuerit litterarum formas
humanitatis per seacula quarta decima et quinta decima. Eodem
modo typi, qui nunc nobis videntur parum clari, fiant sollemnes
in futurum.</p>

<a class="waves-effect waves-light btn modal-trigger center deep-green" href="#modal1">Join Angel Network</a>

<div id="modal1" class="modal">
    <div class="modal-content">
      <div class="row">
    <form class="col s12">
    <div class="row">
      <div class="col s12">
        <h5 class="deep-yellow-text">Please provide the following information.</h5>
     </div>
    </div>
      <div class="row">
        <div class="input-field">
          <input id="first_name" type="text" class="validate  col s6 browser-default" placeholder="First Name">
        </div>
        <div class="input-field">
          <input id="last_name" type="text" class="validate  col s6 browser-default" placeholder="Last Name">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input value="" id="company"  type="text" class="validate  col s12 browser-default" placeholder="Company">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="email" type="email" class="validate  col s12 browser-default" placeholder="Enter Your Email">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="phone" type="text" name="phone" class="validate  col s12 browser-default" placeholder="Phone Number">
        </div>
      </div>
      <div class="row">
      <a href="#!" class="btn btn-small modal-action waves-effect deep-yellow">Submit</a>
      </div>
    </form>
  </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect deep-green-text"><i class="material-icons">close</i></a>
    </div>
  </div>
@endsection