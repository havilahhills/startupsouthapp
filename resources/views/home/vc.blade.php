<?php ?>

@extends('layouts.app')

@section('title', 'VC Tours - #StartupSouth')

@section('content')
<h3 class='left-align'>VC Tours</h3>
<p class='left-align'>If you are a VC looking to discover the next big thing, you may want to consider touring our communities. We make this happen effortlessly - kindly hit the green button below and leave us a note. We'd take it up from there.</p>

<a class='waves-effect waves-light btn modal-trigger center deep-green' href='#modal3'>Book A Tour</a>

<div id="modal3" class="modal">
    <div class="modal-content">
      <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="col s12">
          <h5 class="deep-yellow-text">Please provide the following information</h5>
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="first_name" type="text" class="validate col s6 browser-default" placeholder="First Name">
        </div>
        <div class="input-field">
          <input id="last_name" type="text" class="validate col s6 browser-default" placeholder="Last Name">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input value="" id="city"  type="text" class="validate col s12 browser-default" placeholder="City">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="email" type="text" class="validate col s12 browser-default" placeholder="Contact Address">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="phone-number" type="text" class="validate col s12 browser-default" placeholder="Phone Number">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="password" type="password" class="validate col s12 browser-default" placeholder="Contact Email">
        </div>
      </div>
      <div class="row">
      
      <a href="#!" class="waves-effect waves-light btn btn-small center deep-yellow black-text">Submit</a>
      </div>
    </form>
  </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="waves-effect waves-light modal-close center deep-green-text"><i class="material-icons">close</i></a>
    </div>
  </div>

@endsection