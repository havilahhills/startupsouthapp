<?php ?>

@extends('layouts.app')

@section('title', 'Conferences - #StartupSouth')

@section('content')
<h3 class='left-align'>Conferences</h3>
<p class='left-align'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore
magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
quis nostrud exerci tation ullamcorper suscipit lobortis nisl
ut aliquip ex ea commodo consequat. Duis autem vel eum iriure
dolor in hendrerit in vulputate velit esse molestie consequat,
vel illum dolore eu feugiat nulla facilisis at vero eros et
accumsan et iusto odio dignissim qui blandit praesent luptatum
zzril delenit augue duis dolore te feugait nulla facilisi.
Nam liber tempor cum soluta nobis eleifend option congue
</p>

<div id="" class="col s12">
        
        <div class="row">
            @foreach($conferences as $conference)
            <div class="col s12 m4">
                <div class="card">
                    <div class="card-image">
                        <img src="{{$conference->photoUrl}}">
                    </div>
                    <div class="card-content">
                        <a href="{{$conference->url}}" class="card-title black-text">{{$conference->name}}</a>
                        <p>{{str_limit($conference->description, 100)}}</p>
                        <p class="right-align">
                            <small class="grey-text">
                                {{$community->city}}, {{$conference->state->name}} State
                            </small>
                        </p>
                        <div style="margin-top:1.5em;"> 
                            <div style="display:inline-block; vertical-align: middle; margin-right: 20px">
                                <p>SHARE</p>
                                <p>
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{urlencode($conference->url)}}"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                        <path fill="#000000" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M18,5H15.5A3.5,3.5 0 0,0 12,8.5V11H10V14H12V21H15V14H18V11H15V9A1,1 0 0,1 16,8H18V5Z" />
                                        </svg></a>
                                    <a target="_blank" href="https://twitter.com/home?status={{urlencode($text = $conference->description.' | ' .$community->url)}}"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                        <path fill="#000000" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M17.71,9.33C18.19,8.93 18.75,8.45 19,7.92C18.59,8.13 18.1,8.26 17.56,8.33C18.06,7.97 18.47,7.5 18.68,6.86C18.16,7.14 17.63,7.38 16.97,7.5C15.42,5.63 11.71,7.15 12.37,9.95C9.76,9.79 8.17,8.61 6.85,7.16C6.1,8.38 6.75,10.23 7.64,10.74C7.18,10.71 6.83,10.57 6.5,10.41C6.54,11.95 7.39,12.69 8.58,13.09C8.22,13.16 7.82,13.18 7.44,13.12C7.81,14.19 8.58,14.86 9.9,15C9,15.76 7.34,16.29 6,16.08C7.15,16.81 8.46,17.39 10.28,17.31C14.69,17.11 17.64,13.95 17.71,9.33Z" />
                                        </svg></a>
                                    <a target="_blank" href="https://plus.google.com/share?url={{urlencode($conference->url)}}"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                        <path fill="#000000" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M19.5,12H18V10.5H17V12H15.5V13H17V14.5H18V13H19.5V12M9.65,11.36V12.9H12.22C12.09,13.54 11.45,14.83 9.65,14.83C8.11,14.83 6.89,13.54 6.89,12C6.89,10.46 8.11,9.17 9.65,9.17C10.55,9.17 11.13,9.56 11.45,9.88L12.67,8.72C11.9,7.95 10.87,7.5 9.65,7.5C7.14,7.5 5.15,9.5 5.15,12C5.15,14.5 7.14,16.5 9.65,16.5C12.22,16.5 13.96,14.7 13.96,12.13C13.96,11.81 13.96,11.61 13.89,11.36H9.65Z" />
                                        </svg>
                                    </a>
                                </p>
                            </div>
                            <a class="btn deep-green" target="_self" href="{{$conference->url}}">Details</a>
                        </div> 
                    </div>     
                </div>
            </div>
             @endforeach
        </div>
       
        
        <!--<div class="center-align">
            <p><b>There are no  yet, be the first to add one.</b></p>
            <a class="waves-effect waves-light btn modal-trigger right-align deep-green" href='#modal2'>Add </a>
        </div>-->
        
    </div>
    


<!--<div class='row'>
    
<div class='col s12'>
    <h3>Events</h3>
    <div class='col s4'>
        <br>
      <img src="img/startupsouth.jpg" class="responsive-img"></img>  
    </div>
    <div class='col s8'>
        <p><b>#Startupsouth3</b></p>
        <p><b>Date:</b></p>
        <p><b>Venue:</b></p>
        <a href="#">Visit Website</a>
    </div>
</div>
</div>

<div class='row'>
    
<div class='col s12'>
    
    <div class='col s4'>
        <br>
      <img src="img/startupsouth.jpg" class="responsive-img circle"></img>  
    </div>
    <div class='col s8'>
        <p><b>#Startupsouth2</b></p>
        <p><b>Date:</b></p>
        <p><b>Venue:</b></p>
        <a href="#">Visit Website</a>
    </div>
</div>
</div>

<div class='row'>
    
<div class='col s12'>
    
    <div class='col s4'>
        <br>
      <img src="img/startupsouth.jpg" class="responsive-img"></img>  
    </div>
    <div class='col s8'>
        <p><b>#Startupsouth1</b></p>
        <p><b>Date:</b></p>
        <p><b>Venue:</b></p>
        <a href="#">Visit Website</a>
    </div>
</div>-->
</div>




@endsection