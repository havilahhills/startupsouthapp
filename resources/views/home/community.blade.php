@extends('layouts.app')

@section('title', 'Community - #StartupSouth')

@section('content')
@if(old('status'))
<div class="row notice">
    <div class="col s12 green white-text center-align">
        <p>Your community has been successfully created and pending approval</p>
    </div>
</div>
@endif

<div class="row">
    <div class="col l12 m12 s12">
        <h3 class='left'>Community</h3>
        <a class="waves-effect waves-light btn modal-trigger center deep-green right" href='#modal2'>Add Community</a>
    </div>
</div>
<p class='left-align'>A list of all the clusters, co-working spaces/Hubs and Startup in the south-south and south-east.#Startupsouth Community makes discovery and collaboration across the regions seamless. If you are a community, cluster, co-working space/hub leader or a founder in any of the eleven states of the region - add your community.</p>

@if(is_object($currentGroup))
<h5>{{$currentGroup->name}}</h5>
<p >{{$currentGroup->description}}</p>
@endif

<div class="row">
    <div class="col s12">
        <div class="col s2 m1">
            <p><b>Filter:</b></p>
        </div>
        <div class="col s10 m11">
            <ul class="tabs">
                <li class="tab deep-green-text left"><a class="{{is_object($currentGroup)?'' : 'active'}}" target="_self" href="{{route('home.community.index')}}">All</a></li>
                @foreach($groups as $group)
                <li class="tab deep-green-text left"><a class="{{is_object($currentGroup)?($currentGroup->key === $group->key ?'active' : ''):''}}" target="_self" href="{{route('home.community.index',['tab'=>$group->key])}}">{{$group->name}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    <div id="{{is_object($currentGroup)?$currentGroup->key : 'all'}}" class="col s12">
        @forelse($communities->chunk(3) as $communityRow)
        <div class="row">
            @foreach($communityRow as $community)
            <div class="col s12 m4">
                <div class="card">
                    <div class="card-image">
                        <img src="{{$community->photoUrl}}">
                    </div>
                    <div class="card-content">
                        <a href="{{$community->url}}" class="card-title black-text">{{$community->name}}</a>
                        <p>{{str_limit($community->description, 100)}}</p>
                        <p class="right-align">
                            <small class="grey-text">
                                {{$community->city}}, {{$community->state->name}} State
                            </small>
                        </p>
                        <div style="margin-top:1.5em;"> 
                            <div style="display:inline-block; vertical-align: middle; margin-right: 20px">
                                <p>SHARE</p>
                                <p>
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{urlencode($community->url)}}"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                        <path fill="#000000" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M18,5H15.5A3.5,3.5 0 0,0 12,8.5V11H10V14H12V21H15V14H18V11H15V9A1,1 0 0,1 16,8H18V5Z" />
                                        </svg></a>
                                    <a target="_blank" href="https://twitter.com/home?status={{urlencode($text = $community->description.' | ' .$community->url)}}"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                        <path fill="#000000" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M17.71,9.33C18.19,8.93 18.75,8.45 19,7.92C18.59,8.13 18.1,8.26 17.56,8.33C18.06,7.97 18.47,7.5 18.68,6.86C18.16,7.14 17.63,7.38 16.97,7.5C15.42,5.63 11.71,7.15 12.37,9.95C9.76,9.79 8.17,8.61 6.85,7.16C6.1,8.38 6.75,10.23 7.64,10.74C7.18,10.71 6.83,10.57 6.5,10.41C6.54,11.95 7.39,12.69 8.58,13.09C8.22,13.16 7.82,13.18 7.44,13.12C7.81,14.19 8.58,14.86 9.9,15C9,15.76 7.34,16.29 6,16.08C7.15,16.81 8.46,17.39 10.28,17.31C14.69,17.11 17.64,13.95 17.71,9.33Z" />
                                        </svg></a>
                                    <a target="_blank" href="https://plus.google.com/share?url={{urlencode($community->url)}}"><svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                        <path fill="#000000" d="M5,3H19A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5A2,2 0 0,1 5,3M19.5,12H18V10.5H17V12H15.5V13H17V14.5H18V13H19.5V12M9.65,11.36V12.9H12.22C12.09,13.54 11.45,14.83 9.65,14.83C8.11,14.83 6.89,13.54 6.89,12C6.89,10.46 8.11,9.17 9.65,9.17C10.55,9.17 11.13,9.56 11.45,9.88L12.67,8.72C11.9,7.95 10.87,7.5 9.65,7.5C7.14,7.5 5.15,9.5 5.15,12C5.15,14.5 7.14,16.5 9.65,16.5C12.22,16.5 13.96,14.7 13.96,12.13C13.96,11.81 13.96,11.61 13.89,11.36H9.65Z" />
                                        </svg>
                                    </a>
                                </p>
                            </div>
                            <a class="btn deep-green" target="_self" href="{{$community->url}}">Details</a>
                        </div> 
                    </div>     
                </div>
            </div>
            @endforeach
        </div>
        @empty
        <div class="center-align">
            <p><b>There are no {{is_object($currentGroup)? strtolower($currentGroup->name):'communities'}} yet, be the first to add one.</b></p>
            <a class="waves-effect waves-light btn modal-trigger right-align deep-green" href='#modal2'>Add {{is_object($currentGroup)? strtolower($currentGroup->name):'communities'}}</a>
        </div>
        @endforelse
    </div>
    <div class="col s12">{{$communities->links()}}</div>
</div>


<div id="modal2" class="modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-content">
        <div class="row">
            <div class="col s12">
                <h5 class="deep-yellow-text">
                    <span class="left">Please provide the following information</span>
                    <a href="#!" class="waves-effect waves-light modal-close right deep-green-text">
                        <i class="material-icons">close</i></a>
                </h5>
            </div>
        </div>
        <form action="{{route('home.community.create')}}" onsubmit="return false;"
              method="post" id="addcommunity">
            {{ csrf_field() }}
            <div class="row">
                <div class="col s12">
                    <span class="font-lg">Personal Details</span>
                    <div class="input-field">
                        <input id="first_name" name="first_name" required type="text" class="validate col s12 m6 browser-default" placeholder="First Name">
                    </div>
                    <div class="input-field">
                        <input id="last_name" name="last_name" required type="text" class="validate col s12 m6 browser-default" placeholder="Last Name">
                    </div>
                    <div class="input-field">
                        <input id="phone-number" name="phone" required type="text" class="validate col s12 m6 browser-default" placeholder="Phone Number">
                    </div>
                    <div class="input-field">
                        <input id="email" name="email" required type="text" class="validate col s12 m6 browser-default" placeholder="Contact Email">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <span class="font-lg">Community Details</span>
                    <div class="input-field">
                        <input id="name" name="community_name" required type="text" class="validate col s12 browser-default" placeholder="Name of community">
                    </div>
                    <div class="input-field">
                        <input id="address" name="community_address" required  type="text" class="validate col s12 browser-default" placeholder="Address">
                    </div>
                    <div class="input-field">
                        <input id="city" name="community_city" required  type="text" class="validate col s12 m6 browser-default" placeholder="City">
                    </div>
                    <div class="">
                        <select id="state" name="community_state" required class="validate col s12 m6 browser-default" placeholder="State">
                            <option value="">Select State</option>
                            @foreach($states as $state)
                            <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-field">
                        <input id="website" name="community_website" type="url" class="validate col s12 browser-default" placeholder="Website">
                    </div>
                    <div class="input-field">
                        <textarea id="description" name="community_description" rows="3" required maxlength="1000" class="validate col s12 browser-default" placeholder="Describe your community to us"></textarea>
                    </div>
                    <div>
                        <p class="font-sm">Which group(s) describe your community?</p>
                        @foreach($groups as $group)
                        <input type="checkbox" name="community_groups[]" id="input-{{$group->key}}" value="{{$group->id}}"
                               {{is_object($currentGroup)?($currentGroup->key === $group->key ?'checked="checked"' : ''):''}} />
                        <label for="input-{{$group->key}}">{{$group->name}}</label>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <span class="font-lg">Are you authorized to manage this community?</span>
                    <div class="switch">
                        <label>
                            No
                            <input onchange="switchAdmin()" name="admin" type="checkbox">
                            <span class="lever"></span>
                            Yes
                        </label>
                    </div>
                </div>
            </div>
            <div class="row admin-details">
                <div class="col s12">
                    <span class="font-lg">Administrator Details</span>
                    <div class="input-field">
                        <input id="first_name" name="admin_first_name" required type="text" class="validate col s12 m6 browser-default" placeholder="First Name">
                    </div>
                    <div class="input-field">
                        <input id="last_name" name="admin_last_name" required type="text" class="validate col s12 m6 browser-default" placeholder="Last Name">
                    </div>
                    <div class="input-field">
                        <input id="phone-number" name="admin_phone" required type="text" class="validate col s12 m6 browser-default" placeholder="Phone Number">
                    </div>
                    <div class="input-field">
                        <input id="email" name="admin_email" required type="text" class="validate col s12 m6 browser-default" placeholder="Contact Email">
                    </div>
                </div>
            </div>
            <span class="notify"></span>
            <div class="row">
                <div class="col s12">
                    <span class="working hide left">Saving...</span>
                    <button onclick="addCommunity();" class="btn btn-small waves-effect waves-light modal-trigger right deep-yellow">Continue</button>
                </div>
            </div>
        </form>
        <form id="uploadphoto" class="animated slideInRight hide" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <h3>Alright! Let's choose a picture for your community</h3>
            <div class="input-field">
                <input id="photo" name="community_photo" type="file" class="validate col s12 browser-default" placeholder="Select profile picture">
            </div>
            <div class="row">
                <div class="col s12">
                    <span class="working hide left">Uploading...</span>
                    <button onclick="$(this).prev('span').removeClass('hide')" 
                            class="btn btn-small waves-effect waves-light modal-trigger right deep-yellow">Upload</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--<div class='row'>
    
<div class='col s12'>
    
    <div class='col s4'>
        <br>
      <img src="img/background2.jpg" class="responsive-img"></img>  
    </div>
    <div class='col s8'>
        <p><b>XYZ Hub</b></p>
        <p><b>City:</b></p>
        <p><b>Description:</b>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore
magna aliquam erat volutpat</p>
        <a href="#">Visit Website</a>
    </div>
</div>
</div>

<div class='row'>
    
<div class='col s12'>
    
    <div class='col s4'>
        <br>
      <img src="img/background2.jpg" class="responsive-img"></img>  
    </div>
    <div class='col s8'>
        <p><b>XYZ Hub</b></p>
        <p><b>City:</b></p>
        <p><b>Description:</b></p>
        <a href="#">Visit Website</a>
    </div>
</div>
</div>

<div class='row'>
    
<div class='col s12'>
    
    <div class='col s4'>
        <br>
      <img src="img/background2.jpg" class="responsive-img"></img>  
    </div>
    <div class='col s8'>
        <p><b>XYZ Hub</b></p>
        <p><b>City:</b></p>
        <p><b>Description:</b></p>
        <a href="#">Visit Website</a>
    </div>
</div>
</div>-->

@endsection

@section('scripts')
@parent
<script src="{{asset('js/utils.js')}}"></script>
<script type="text/javascript">
                        function switchAdmin() {
                            var form = $('#addcommunity');
                            var row = $('.row.admin-details', form);
                            var admin = $('input[name=admin]', form).prop('checked');
                            if (admin) {
                                row.slideUp();
                                $('input', row).prop('required', false);
                            } else {
                                row.slideDown();
                                $('input', row).prop('required', true);
                            }
                        }

                        var community = 0;
                        function addCommunity() {
                            var form = $('#addcommunity');
                            var modal = $('#modal2');
                            $('button', modal).prop('disabled', true);
                            $('.working', modal).removeClass('hide');
                            ajaxCall({
                                'url': form.attr('action'),
                                'method': 'POST',
                                'data': form.serialize(),
                                'onSuccess': function (xhr) {
                                    $("#uploadphoto").attr('action', "{{route('home.community.photo')}}?id=" + xhr);
                                    form.addClass('hide animated slideOutLeft');
                                    $('#uploadphoto').removeClass('hide');
                                },
                                'onFailure': function (xhr) {
                                    handleHttpErrors(xhr, form);
                                },
                                'onComplete': function (xhr) {
                                    $('.working', modal).addClass('hide');
                                    $('button', modal).prop('disabled', false);
                                }
                            });
                            return false;
                        }

                        $(function () {
                            setTimeout(function () {
                                $('.row.notice').addClass('animated zoomOut').hide();
                            }, 10000);
                        });

</script>
@endsection