<?php ?>

@extends('layouts.header')

@section('title', 'South-South and South-East, Nigeria Tech Ecosystem - #StartupSouth')

@section('post_content')


<div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send green-text text-lighten-1"></i></h3>
          <h4 class="home-lead">Some Awesome Speakers/Mentors</h4>
          <section id="photos">
            <img src="img/speaker.jpg" alt="Cute cat">
            <img src="img/mark-2.jpg">
            <img src="img/speaker14.jpg">
            <img src="img/izobe.jpg">
            <img src="img/lexi-novitske-s.jpg">
            <img src="img/funke-tp.jpg">
            <img src="img/iyin.jpg">
            <img src="img/ononobi.jpg">
            <img src="img/speaker10.jpg">
            <img src="img/speaker11.png">
            <img src="img/chukwuemeka.jpg">
            <img src="img/chinenye.jpg">
            <img src="img/olaotan.jpg">
            <img src="img/ngozi-dozie.png">
          </section>
        </div>
        <div>
          
        </div>
        
        <div class="row center">
          <a href="#" id="download-button" class="btn-large waves-effect waves-light deep-yellow black-text">Learn More</a>
        </div>
      </div>



@endsection