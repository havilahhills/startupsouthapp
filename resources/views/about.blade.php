<?php ?>

@extends('layouts.app')

@section('title', '#StartupSouth - About')

@section('content')
<h5>About #StartupSouth</h5>
<p>#StartupSouth started in 2015 as an annual Startup Conference for Startup Founders, Investors and the general public in the South-South and South-East of Nigeria. It features Fireside chats with Successful Startup Founders, Policy Makers and Investors from across the world, Pitch Sessions and Networking.
<h6><b>How #StartupSouth Started</b></h6>

<p>It started as an event featuring education (via Fireside chats and Panel Discussions with experienced entrepreneurs from all around the world), Networking Opportunities, A Startup Pitch and Dinner.<br>Pre-selected Startup teams working on brilliant, tech-based ideas were trained and thereafter engaged in a competitive pitch in order to attract possible investment by investors.<br>Notable names in the Nigerian Tech Ecosystem that has graced the event include Eyin Aboyeji (Former Andela and Currently, CEO, Flutterwave), Mark Essien (Hotels.ng), Simeon Ononubi (Simplepay & MyAds Global), Lexi Novitske (Singularity Investments), Editi Effiong (Anakle), Towry-Coker Olaotan (Afritickets and Cranium One) to mention just a few.</p>
<h6><b>The Mission/Goal</b></h6>
The goal of #Startupsouth is to encourage innovation, entrepreneurial solutions (with a tech bias) and spotlight startup founders and teams operating in the eleven states of south-south and south-east geo-political regions of Nigeria. 

<h6><b>Our Belief</b></h6>
<ul class="b">
    <li>We believe that Nigeria (by extension Africa) will only be sustainable  if more states and cities become sustainable.</li>
    <li>We believe that Entrepreneurs are better motivated to solve socio-economic problems than Government</li>
    <li>We believe that Young People hold the key to the Economic progress of Nigeria.</li>
    <li>We believe we can stimulate, harness and replicate the entrepreneurial successes of the leading states/cities across the nation starting from The South-south and South-East.</li>
</ul>
 




<h6><b>Why Regional?</b></h6>
<p>The Two Regions(South-South and South-East) combined will create the critical population mass necessary for validating and scaling innovations.<br>The other reason is that these regions are currently grossly underserved by opportunities in the national scheme of things including flow of investments, training and exposure.</p>
<h6><b>Additional Ecosystem Engagement</b></h6><p>#StartupSouth evolved to include other region focused ecosystem building engagements. 
In 2017 alone, we have hosted high profile Tech and Entrepreneurship figures across Enugu, Nsukka, Port Harcourt, Uyo and Umuahia including:
</p>
<ul>
    <li>Chukwuemeka Afigbo, Head, Platform Partnerships for Middle East, Asia & Africa at Facebook and others</li>
    <li>Also, hosted the Ventures Platform VC Tour that resulted in two teams from the regions being selected for a $20,000 investment.</li>
</ul>
<h6><b>Some Progress Made So Far</b></h6>
<ul>
    <li>
        Over 70 Startup Teams and over 5000 Participants have been reached and engaged since inception.
    </li>
    <li>Two (2) Teams from the Region have received $20,000 (USD) and are part of the Cohort 2 of  the Ventures Platform Accelerator, Abuja.</li>
    <li>In recognition of the work done so far, our founder Uche Aniche was Awarded a PEN Youth Impact Award by Professionals Engagement Network</li>
</ul>
<h6><b>Why We Need Volunteers</b></h6>
<ul>
    <li>To join our workforce as we host the 3rd edition of #startupsouth while also learning new skills,experience and knowledge.</li>
    <li>To join our team in making a difference in all of the #startupsouth regions</li>
    <li>To create a network of like minds passionate about  innovations and entrepreneurial development while achieving the overall goal for #startupsouth3.</li>
</ul>

<p></p>

@endsection
