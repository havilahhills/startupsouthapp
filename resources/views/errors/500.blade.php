<?php ?>

@extends('layouts.app')

@section('title', 'Oops! Something went wrong - #StartupSouth')

@section('content')

<h2 class='center'>Something went wrong :(</h2>
<p class='center'>This is unusual. Somehow, something unexpected happened, please try again</p>

@endsection