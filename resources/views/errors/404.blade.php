<?php ?>

@extends('layouts.app')

@section('title', 'Page Not Found - #StartupSouth')

@section('content')

<h2 class='center'>Page Not Found</h2>
<p class='center'>You may have followed a wrong link. 
    <a class="green-text" href="{{url('/')}}">Click Here to go back </a> to the home page</p>

@endsection