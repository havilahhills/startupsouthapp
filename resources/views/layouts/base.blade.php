<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/favicon.ico')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/favicon.ico')}}">
        <link rel="icon" type="image/png" href="{{asset('img/favicon.ico')}}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{asset('img/favicon.ico')}}" sizes="16x16">

        <link rel="shortcut icon" href="img/favicon.ico">

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{asset('css/animate.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="https://fonts.googleapis.com/css?family=Bitter:700" rel="stylesheet">
        <script>
            window.Laravel = <?= json_encode(['csrfToken' => csrf_token()]); ?>;
            window.HomeUrl = '<?= rtrim(url('/'), '/') . '/'; ?>';
            window.env = '<?= app()->environment(); ?>';
        </script>

        <?php
        $defaultTitle = config('app.name');
        $defaultDescription = '';
        $defaultImage = asset('img/startupsouth.jpg');
        ?>
        <title>@yield('title',$defaultTitle)</title>
        <meta name="description" content="@yield('description',$defaultDescription)">
        <!--Twitter card-->
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:domain" content="{{url('/')}}"/>
        <meta name="twitter:description" content="@yield('description',$defaultDescription)"/>
        <meta name="twitter:title" content="@yield('title',$defaultTitle)"/>
        <meta name="twitter:image" content="@yield('image',$defaultImage)"/>
        <!--Open graph-->
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:type" content="website"/>
        <meta property="og:description" content="@yield('description',$defaultDescription)"/>
        <meta property="og:title" content="@yield('title',$defaultTitle)"/>
        <meta property="og:image" content="@yield('image',$defaultImage)"/>
    </head>
    <body>

        <div class="navbar-fixed">
            <ul id="dropdown1" class="dropdown-content">
                <li><a href="{{ url('about') }}">History</a></li>
                <li><a href="#!">Why Regional</a></li>
                <li><a href="#!">Speakers and Mentors</a></li>
                <li><a href="#!">Partners</a></li>
                <li><a href="#!">Team</a></li>
                <li><a href="#!">Contact</a></li>
            </ul>
            <ul id="dropdown2" class="dropdown-content">
                <li><a href="#!">#StartupSouth1</a></li>
                <li><a href="#!">#StartupSouth2</a></li>
                <li><a href="#!">#StartupSouth3</a></li>
            </ul>
            <ul id="dropdown3" class="dropdown-content">
                <li><a href="{{route('home.community.index')}}">Communities</a></li>
                <li><a href="#!">Events</a></li>
                <li><a href="#!">Forum</a></li>
            </ul>

            <ul id="dropdown4" class="dropdown-content">
                <li><a href="#!">History</a></li>
                <li><a href="#!">Why Regional</a></li>
                <li><a href="#!">Speakers and Mentors</a></li>
                <li><a href="#!">Partners</a></li>
                <li><a href="#!">Team</a></li>
                <li><a href="#!">Contact</a></li>
            </ul>
            <ul id="dropdown5" class="dropdown-content">
                <li><a href="#!">#StartupSouth1</a></li>
                <li><a href="#!">#StartupSouth2</a></li>
                <li><a href="#!">#StartupSouth3</a></li>
            </ul>
            <ul id="dropdown6" class="dropdown-content">
                <li><a href="{{route('home.community.index')}}">Communities</a></li>
                <li><a href="#!">Events</a></li>
                <li><a href="#!">Forum</a></li>
            </ul>
            <nav class="transparent" role="navigation">
                <div class="nav-wrapper nav-container-fluid">
                    <ul id="nav-mobile" class="side-nav">
                        <li><a id="homepage" href="{{url('/')}}">Home</a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown4">About<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown5">Conference<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown6">Community<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a href="#">Blog</a></li>
                        <li class="deep-yellow"><a class="black-text" href="#">Attend/Pitch</a></li>
                    </ul>
                    <a id="logo-container" href="#" class="brand-logo">
                        <img class="hide-on-med-and-down" src="{{asset('img/startupsouth_logo.png')}}" />
                        <img class="hide-on-large-only" src="{{asset('img/startupsouth_logo.png')}}" width="220px" height="auto" />
                    </a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown1">About<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Conference<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a class="dropdown-button" href="#!" data-activates="dropdown3">Community<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="{{ url('/new-login') }}">Sign In/Sign Up</a></li>
                        <li class="deep-yellow"><a class="black-text" href="#">Attend/Pitch</a></li>
                    </ul>
                    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons menu deep-yellow-text">menu</i></a>
                </div>
            </nav>
        </div>

        @yield('hero')

        <div id="index-region" class="parallax-container valign-wrapper">
            <div class="section no-pad-bot">
                <div class="container">
                    <div class="row center">
                        <h5 class="header region home-lead col s12 deep-green-text">11 States, 13 Cities, 650+KM<sup>2</sup> and over 50Million People.</h5>
                    </div>
                </div>
            </div>
            <div class="parallax"><!--<img src="img/background2.jpg" alt="Unsplashed background img 2"> --> <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1015710.1827876485!2d7.2059022421874985!3d6.047045513919724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sng!4v1502310942290" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
        </div>

        @yield('post_content')

        <div id="index-contact" class="parallax-container valign-wrapper">
            <div class="section no-pad-bot">
                <div class="container">
                    <div class="row">
                        <h5 class="header col s12 light center-align">Stay connected, subscribe to our newsletter.</h5>
                        <form action="#" onclick="return false;" id="home-newsletter" class="col s12 m8 offset-m2">
                            <row class="">
                                <div class="input-field home-newsletter">
                                    <input class="col m8 s12 browser-default newsletter" placeholder="yourname@example.com" id="name" type="email" require>
                                </div>
                                <div class="input-field">
                                    <input type="submit" role="button" class="btn main-search deep-yellow black-text col l4 m4 s12"  value="Submit">
                                </div>
                            </row>
                        </form>
                    </div>
                </div>
            </div>
            <div class="parallax"><img src="{{asset('img/startupsouth-base.jpg')}}" alt="Unsplashed background img 3"></div>
        </div>

        <footer class="page-footer deep-green">
            <div class="container footer-container-fluid">
                <div class="row">
                    <div class="col l5 s12">
                        <h6 class="white-text home-lead">About #StartupSouth</h6>
                        <p class="grey-text text-lighten-4">The simple goal of #StartupSouth is to Connect, Expose and Attract Investment to Startups and Founders residing and operating out of South-South and South-East Nigeria.</p>


                    </div>
                    <div class="col l4 s12">
                        <h6 class="white-text home-lead">Explore</h6>
                        <ul>
                            <li><a class="white-text" href="{{route('home.community.index',['tab'=>'startups'])}}">#StartupSouth Startups</a></li>
                            <!--                            <li><a class="white-text" href="#!">Tech Events in The Niger Delta</a></li>
                                                        <li><a class="white-text" href="#!">Tech Events in South-East</a></li>-->
                            <li><a class="white-text" href="{{route('home.community.index')}}">Explore Our Community</a></li>
                            <li><a class="white-text" href="{{route('home.community.index',['tab'=>'hubs'])}}">Hubs/Co-Working Spaces</a></li>
                            <li><a class="white-text" href="#!">Contact #StartupSouth</a></li>
                        </ul>
                    </div>
                    <div class="col l3 s12">
                        <h6 class="white-text home-lead">Connect</h6>
                        <ul class="inline">
                            <li><a class="white-text" href="#!"><img src="{{asset('img/facebook-logo.png')}}" width="45px" height="45px"/></a></li>
                            <li><a class="white-text" href="#!"><img src="{{asset('img/instagram-logo.png')}}" width="45px" height="45px"/></a></li>
                            <li><a class="white-text" href="#!"><img src="{{asset('img/twitter-logo.png')}}" width="45px" height="45px"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container footer-container-fluid">
                    Made with <svg style="width:24px;height:24px; margin-top:100px;" viewBox="0 0 24 24">
                    <path fill="#ffc400" d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" />
                    </svg> by <a class="green-text text-lighten-1 text-lighten-3" href="#">Havilah & Hills</a>
                </div>
            </div>
        </footer>


        <!--  Scripts-->
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="{{asset('js/materialize.js')}}"></script>
        <script src="{{asset('js/init.js')}}"></script>
        @yield('scripts')
    </body>
</html>
