@extends('layouts.base')
@section('hero')
<div id="index-banner-app" class="parallax-container">
    <div class="section no-pad-bot">
        <!--  <div class="container">
            <br><br>
            <h1 class="header center deep-yellow-text text-lighten-1" style="opacity:0.95;">Awesome's Borderless...</h1>
            <div class="row center">
              <h5 class="header col s12 light">Join Over 5000 of tech's best people from across Nigeria and beyond at <b>#StartupSouth</b></h5>
            </div>
            <div class="row center">
              <a href="#" id="download-button" class="btn-large waves-effect waves-light deep-yellow black-text">Attend A #StartupSouth Event</a>
            </div>

          </div> -->
    </div>
    <div class="parallax"><img src="{{asset('img/startupsouth.jpg')}}" alt="Unsplashed background img 1"></div>
</div>


<div class="container">
    <div class="section">
        @yield('content')
        <!--   Icon Section   -->
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
// the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
        $('.modal').modal();
        $('select').material_select();
    });
</script>
<script>
    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 150;
    var navbarHeight = $('nav').outerHeight();

    $(window).scroll(function (event) {
        didScroll = true;
    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 150);

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight) {
            // Scroll Down
            $('nav').removeClass('transparent').addClass('deep-green');
        } else {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                $('nav').removeClass('deep-green').addClass('transparent');
            }
        }

        lastScrollTop = st;
    }

</script>
@endsection