@extends('layouts.base')
@section('hero')
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>
            <h1 class="header center deep-yellow-text text-lighten-1" style="opacity:0.95;">Awesome's Borderless...</h1>
            <div class="row center">
                <h5 class="header col s12 light">Join Over 5000 of tech's best people from across Nigeria and beyond at <b>#StartupSouth</b></h5>
            </div>
            <div class="row center">
                <a href="http://goo.gl/R33tfB" id="download-button" class="btn-large waves-effect waves-light deep-yellow black-text">Attend A #StartupSouth Event</a>
            </div>

        </div>
    </div>
    <div class="parallax"><img src="{{asset('img/startupsouth.jpg')}}" alt="Unsplashed background img 1"></div>
</div>


<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m3">
                <div class="icon-block">
                    <h2 class="center green-text text-lighten-1"><i class="material-icons">flash_on</i></h2>
                    <h5 class="center home-lead">Conference</h5>

                    <p class="light center">#StartupSouth Conference is an annual gathering of successful and aspiring Startup founders, Investors, Policy makers and general stakeholders.
                    </p>
                    <div class="center">
                        <a href="#" id="download-button" class="btn waves-effect waves-light green lighten-1">Learn More</a>
                    </div>
                </div>
            </div>

            <div class="col s12 m3">
                <div class="icon-block">
                    <h2 class="center green-text text-lighten-1"><i class="material-icons">settings</i></h2>
                    <h5 class="center home-lead">Angel Network</h5>

                    <p class="light center">Join smart investors taking a bet a the most daring innovators for super returns. Join or start a new angel investment club. Connect with like minded investors.
                    </p>      <div class="center">
                        <a href="{{ route('home.angel-network') }}" id="download-button" class="btn waves-effect waves-light green lighten-1">Learn More</a>
                    </div>
                </div>
            </div>

            <div class="col s12 m3">
                <div class="icon-block">
                    <h2 class="center green-text text-lighten-1"><i class="material-icons">group</i></h2>
                    <h5 class="center home-lead">Community</h5>

                    <p class="light center">#StartupSouth is about linking communities and clusters. Have a cluster or community active in any target state/city? Link up with the rest of the world.
                    </p>      <div class="center">
                        <a href="{{ route('home.community.index') }}" id="download-button" class="btn waves-effect waves-light green lighten-1">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col s12 m3">
                <div class="icon-block">
                    <h2 class="center green-text text-lighten-1"><i class="material-icons">directions_bus</i></h2>
                    <h5 class="center home-lead">VCTours</h5>

                    <p class="light center">We know how hard it can be to keep track of the next big thing. We know our community better than anyone else, let’s guide you on a tour.</p>      
                    <div class="center">
                        <a href="{{ route('home.vc-tours') }}" id="download-button" class="btn waves-effect waves-light green lighten-1">Learn More</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')
<script>
    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 500;
    var navbarHeight = $('nav').outerHeight();

    $(window).scroll(function (event) {
        didScroll = true;
    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight) {
            // Scroll Down
            $('nav').removeClass('transparent').addClass('deep-green');
        } else {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                $('nav').removeClass('deep-green').addClass('transparent');
            }
        }

        lastScrollTop = st;
    }

    $(document).ready(function () {
        var homeUrl = 'https://startupsouth-havilah.c9users.io/parallax-template';

        if (document.URL == homeUrl) {
            var d = document.getElementById("index-banner");
            d.className += "";
        } else {
            var d = document.getElementById("index-banner");
            d.className += " general";
        }
    });
</script>
@endsection
