
@extends('layouts.app')

@section('title', $user->name. ' - #StartupSouth')
@section('image', $user->photoUrl)
@section('description', $user->description)

@section('content')
<h3 class='left-align'>{{$user->name}}</h3>
<p class='left-align'>{{$user->bio}}</p>

<a class="waves-effect waves-light btn modal-trigger center deep-green" href="#modal1">Join Angel Network</a>

<div id="modal1" class="modal">
    <div class="modal-content">
      <div class="row">
    <form class="col s12">
    <div class="row">
      <div class="col s12">
        <h5 class="deep-yellow-text">Please provide the following information.</h5>
     </div>
    </div>
      <div class="row">
        <div class="input-field">
          <input id="first_name" type="text" class="validate  col s6 browser-default" placeholder="First Name">
        </div>
        <div class="input-field">
          <input id="last_name" type="text" class="validate  col s6 browser-default" placeholder="Last Name">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input value="" id="company"  type="text" class="validate  col s12 browser-default" placeholder="Company">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="email" type="email" class="validate  col s12 browser-default" placeholder="Enter Your Email">
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <input id="phone" type="text" name="phone" class="validate  col s12 browser-default" placeholder="Phone Number">
        </div>
      </div>
      <div class="row">
      <a href="#!" class="btn btn-small modal-action waves-effect deep-yellow">Submit</a>
      </div>
    </form>
  </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect deep-green-text"><i class="material-icons">close</i></a>
    </div>
  </div>
@endsection