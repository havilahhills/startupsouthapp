@if ($paginator->hasPages())
<ul class="pagination">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <li><button class="btn btn-flat disabled"><i class="material-icons">chevron_left</i></button></li>
    @else
    <li><a class="btn btn-flat  deep-green-text" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="material-icons">chevron_left</i></a></li>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
    {{-- "Three Dots" Separator --}}
    @if (is_string($element))
    <li class="disabled"><span>{{ $element }}</span></li>
    @endif

    {{-- Array Of Links --}}
    @if (is_array($element))
    @foreach ($element as $page => $url)
    @if ($page == $paginator->currentPage())
    <li><button class="btn btn-flat  deep-green white-text active">{{ $page }}</button></li>
    @else
    <li><a class="btn btn-flat  deep-green-text" href="{{ $url }}">{{ $page }}</a></li>
    @endif
    @endforeach
    @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li><a class="btn btn-flat  deep-green-text" href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="material-icons">chevron_right</i></a></li>
    @else
    <li><button class="btn btn-flat disabled"><i class="material-icons">chevron_right</i></button></li>
    @endif
</ul>
@endif
